package br.com.hellopetdesign.hellopetvendas.activities

import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import android.view.View
import android.widget.TextView
import br.com.hellopetdesign.hellopetvendas.R
import br.com.hellopetdesign.hellopetvendas.adapters.OrderAdapter
import br.com.hellopetdesign.hellopetvendas.utils.PasswordManager
import br.com.hellopetdesign.hellopetvendas.viewmodels.OrderViewModel
import kotlinx.android.synthetic.main.activity_order_list.*
import java.text.NumberFormat

class OrderListActivity : AppCompatActivity() {
    lateinit var adapter : OrderAdapter
    lateinit var orderViewModel : OrderViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_order_list)

        if (PasswordManager.isAdmin()) {
            orderFilterContainer.visibility = View.VISIBLE
        }

        adapter = OrderAdapter(this@OrderListActivity)

        orderViewModel = ViewModelProviders.of(this).get(OrderViewModel::class.java)
        orderViewModel.orders.observe(this, Observer {
            adapter.updateOrders(it!!)
            var sum = 0.0
            for (order in it) {
                if (order.orderNum > 1566405123309) sum += order.getSumPrices()
            }
            orderListTotal.text = (NumberFormat.getCurrencyInstance().format(sum))
        })

        orderList.adapter = adapter

        if (PasswordManager.isAdmin()) orderViewModel.updateOrders("Todos")
        else orderViewModel.updateOrders(PasswordManager.currentUser())
    }

    override fun onResume() {
        super.onResume()

        adapter.notifyDataSetChanged()
    }

    fun onClickFilter(view: View) {
        val builder = AlertDialog.Builder(this@OrderListActivity)
        builder.setTitle("Escolha o filtro")
        val filters = PasswordManager.USERS.plus("Todos")
        builder.setSingleChoiceItems(filters, 0) { dialogInterface, i ->
            orderViewModel.updateOrders(filters[i])
            (view as TextView).text = filters[i]
            dialogInterface.dismiss()
        }
        builder.create().show()
    }
}