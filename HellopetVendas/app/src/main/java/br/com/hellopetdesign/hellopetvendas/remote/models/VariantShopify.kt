package br.com.hellopetdesign.hellopetvendas.remote.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class VariantShopify(
        val id: Long,
        val barcode: String = "00000000",
        @SerializedName("title") var name: String,
        @SerializedName("sku") val ref: String = "default",
        val price: Double = 0.00) : Serializable {
}