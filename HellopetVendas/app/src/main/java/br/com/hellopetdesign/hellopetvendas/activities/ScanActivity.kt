package br.com.hellopetdesign.hellopetvendas.activities

import android.content.DialogInterface
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.util.SparseArray
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import br.com.hellopetdesign.hellopetvendas.R
import br.com.hellopetdesign.hellopetvendas.database.AppDatabase
import br.com.hellopetdesign.hellopetvendas.models.Product
import br.com.hellopetdesign.hellopetvendas.models.ProductInOrder
import br.com.hellopetdesign.hellopetvendas.viewmodels.ProductsViewModel
import com.google.android.gms.vision.barcode.Barcode
import info.androidhive.barcode.BarcodeReader
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class ScanActivity : AppCompatActivity() {
    private var handler = Handler()
    private var runnable = Runnable {}

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_scan)

        val barcodeReader = supportFragmentManager.findFragmentById(R.id.barcode_fragment) as BarcodeReader


        barcodeReader.setListener(object : BarcodeReader.BarcodeReaderListener {
            override fun onBitmapScanned(sparseArray: SparseArray<Barcode>?) {}

            override fun onScannedMultiple(barcodes: MutableList<Barcode>?) {
                handler.removeCallbacks(runnable)
                barcodeReader.pauseScanning()

                GlobalScope.launch {
                    val products = barcodes!!.map {
                        val prod = AppDatabase(this@ScanActivity).variantShopifyDao().getVariantFromBarcode(it.displayValue.replace(" ", ""))
                        if (prod == null)
                            return@map "Código ${it.displayValue} não encontrado"
                        else
                            return@map prod.title
                    }

                    runOnUiThread {
                        val alertDialogBuilder = AlertDialog.Builder(this@ScanActivity)
                        alertDialogBuilder.setTitle("ESCOLHA O PRODUTO")
                        alertDialogBuilder.setSingleChoiceItems(products.toTypedArray(), 0) { dialogInterface: DialogInterface?, i: Int ->
                            readBardcode(barcodes[i], barcodeReader)
                            dialogInterface!!.dismiss()
                        }
                        alertDialogBuilder.setOnCancelListener {
                            barcodeReader.resumeScanning()
                        }
                        alertDialogBuilder.create().show()
                    }
                }
            }

            override fun onCameraPermissionDenied() {
                Toast.makeText(applicationContext,
                        "Por favor aceite a permissão para sc" +
                                "annear",
                        Toast.LENGTH_SHORT).show()
            }

            override fun onScanned(barcode: Barcode?) {
                handler.removeCallbacks(runnable)
                runnable = Runnable {
                    barcodeReader.pauseScanning()
                    readBardcode(barcode, barcodeReader)
                }
                handler.postDelayed(runnable, 1000)
            }

            override fun onScanError(errorMessage: String?) {
                Log.e("vgc", errorMessage)
            }
        })
    }

    private fun readBardcode(barcode: Barcode?, barcodeReader: BarcodeReader) {
        barcodeReader.playBeep()

        GlobalScope.launch {
            val db = AppDatabase(this@ScanActivity)
            val variantShopifyEntity = db.variantShopifyDao().getVariantFromBarcode(barcode!!.displayValue)

            @Suppress("SENSELESS_COMPARISON")
            if (variantShopifyEntity == null) {
                runOnUiThread {
                    Toast.makeText(this@ScanActivity,
                            "Produto não encontrado",
                            Toast.LENGTH_SHORT).show()
                    barcodeReader.pauseScanning()
                    Handler().postDelayed({
                        barcodeReader.resumeScanning()
                    }, 500)
                }
            } else {
                val productShopifyEntity = db.productShopifyDao().getProduct(variantShopifyEntity.prodId)

                val product = Product(barcode = variantShopifyEntity.barcode,
                        name = variantShopifyEntity.title!!,
                        ref = variantShopifyEntity.sku,
                        price = variantShopifyEntity.price!!,
                        imageUrl = productShopifyEntity.imageUrl)

                runOnUiThread {
                    barcodeReader.pauseScanning()

                    val editText = EditText(this@ScanActivity)
                    editText.width = 300

                    val builder = AlertDialog.Builder(this@ScanActivity)
                    builder.setTitle("OBSERVAÇÕES")
                    builder.setCancelable(false)
                    builder.setView(editText)
                    builder.setPositiveButton(android.R.string.ok) { dialogInterface, _ ->
                        OrderActivity.order.products.add(ProductInOrder(product,
                                1,
                                editText.text.toString()))
                        dialogInterface.dismiss()
                        this@ScanActivity.finish()
                    }
                    builder.create().show()
                }
            }
        }
    }
}