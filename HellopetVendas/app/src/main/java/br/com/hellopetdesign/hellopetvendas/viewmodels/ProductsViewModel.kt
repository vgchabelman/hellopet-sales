package br.com.hellopetdesign.hellopetvendas.viewmodels

import br.com.hellopetdesign.hellopetvendas.models.Product
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener


class ProductsViewModel {
    companion object {
        var products = HashMap<String, Product>()

        fun updateProducts() {
            val database = FirebaseDatabase.getInstance()
            val myRef = database.getReference("products")
            myRef.addValueEventListener(object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    val contactChildren = snapshot.children
                    val temp = HashMap<String, Product>()

                    for (contact in contactChildren) {
                        val c = contact.getValue<Product>(Product::class.java)
                        c?.let { temp.put(it.barcode.replace(" ", ""), it) }
                    }

                    products = HashMap(temp)
                }

                override fun onCancelled(error: DatabaseError) {}
            })
        }
    }
}