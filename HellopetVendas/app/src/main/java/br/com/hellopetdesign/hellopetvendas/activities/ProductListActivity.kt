package br.com.hellopetdesign.hellopetvendas.activities

import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import br.com.hellopetdesign.hellopetvendas.R
import br.com.hellopetdesign.hellopetvendas.adapters.ProductShopifyAdapter
import br.com.hellopetdesign.hellopetvendas.viewmodels.ProductShopifyViewModel
import kotlinx.android.synthetic.main.activity_product_list.*

class ProductListActivity : AppCompatActivity(){
    lateinit var adapter : ProductShopifyAdapter
    lateinit var productsViewModel: ProductShopifyViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_product_list)

        productsViewModel = ViewModelProviders.of(this).get(ProductShopifyViewModel::class.java)
        productsViewModel.products.observe(this, Observer {
            adapter.updateProductList(it!!)
        })

        adapter = ProductShopifyAdapter()
        rvProduct.adapter = adapter

        productsViewModel.updateProdutsShopifyOffline(this)
    }

    override fun onResume() {
        super.onResume()

        adapter.notifyDataSetChanged()
    }
}