package br.com.hellopetdesign.hellopetvendas.viewmodels

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import br.com.hellopetdesign.hellopetvendas.database.AppDatabase
import br.com.hellopetdesign.hellopetvendas.remote.models.ProductShopify
import br.com.hellopetdesign.hellopetvendas.remote.models.VariantShopify
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class ProductShopifyViewModel : ViewModel() {
    val products = MutableLiveData<List<ProductShopify>>()

//    fun updateProductsShopify() {
//        val retrofit = RetrofitUtils.retrofit()
//
//        val shopifyService = retrofit.create(ShopifyService::class.java)
//        shopifyService.listProducts().enqueue(object : Callback<ProductListShopify> {
//            override fun onFailure(call: Call<ProductListShopify>, t: Throwable) {
//                Log.d("vgc", "error" + t.message)
//            }
//
//            override fun onResponse(call: Call<ProductListShopify>, response: Response<ProductListShopify>) {
//                val temp = response.body()
//                if (temp != null) {
//                    for (prod in temp.productList) {
//                        for (variant in prod.variantList) {
//                            variant.name = prod.name + ' ' + variant.name
//                        }
//                    }
//                    products.postValue(temp.productList)
//                }
//            }
//        })
//    }

    fun updateProdutsShopifyOffline(context: Context) {
        GlobalScope.launch {
            val db =  AppDatabase(context)

            val temp = db.productShopifyDao().getAll()

            val convert = ArrayList<ProductShopify>()

            for (prod in temp) {
                val variantShopifyEntitiyList = db.variantShopifyDao().getVariantsFromProduct(prod.id)

                val variantList = ArrayList<VariantShopify>()
                for (vari in variantShopifyEntitiyList) {
                    variantList.add(VariantShopify(vari.id, vari.barcode, vari.title!!, vari.sku, vari.price!!))
                }

                val product = ProductShopify(prod.id, prod.title!!, "", variantList, null, prod.imageUrl!!)
                convert.add(product)
            }

            products.postValue(convert)
        }
    }
}