package br.com.hellopetdesign.hellopetvendas.models

data class ImageShopify(val src : String) {
}

//"image": {
//                "id": 4349347725348,
//                "product_id": 1119981535268,
//                "position": 1,
//                "created_at": "2019-03-19T14:08:39-03:00",
//                "updated_at": "2019-03-19T14:08:51-03:00",
//                "alt": null,
//                "width": 3077,
//                "height": 3250,
//                "src": "https://cdn.shopify.com/s/files/1/0064/1843/2036/products/AR01_COM_PET.jpg?v=1553015331",
//                "variant_ids": [],
//                "admin_graphql_api_id": "gid://shopify/ProductImage/4349347725348"
//            }