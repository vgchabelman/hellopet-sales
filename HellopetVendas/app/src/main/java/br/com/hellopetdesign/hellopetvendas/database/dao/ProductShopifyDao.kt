package br.com.hellopetdesign.hellopetvendas.database.dao

import android.database.sqlite.SQLiteDatabase
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import br.com.hellopetdesign.hellopetvendas.database.model.ProductShopifyEntity

@Dao
interface ProductShopifyDao {
    @Insert(onConflict = SQLiteDatabase.CONFLICT_REPLACE)
    suspend fun insert(productShopifyEntity: ProductShopifyEntity)

    @Insert(onConflict = SQLiteDatabase.CONFLICT_REPLACE)
    suspend fun insert(productShopifyEntity: List<ProductShopifyEntity>)

    @Query("SELECT * FROM ProductShopifyEntity WHERE id = :id")
    suspend fun getProduct(id: Long) : ProductShopifyEntity

    @Query("SELECT * FROM ProductShopifyEntity")
    suspend fun getAll() : List<ProductShopifyEntity>
}