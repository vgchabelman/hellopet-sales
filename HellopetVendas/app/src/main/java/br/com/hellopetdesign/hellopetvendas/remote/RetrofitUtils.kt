package br.com.hellopetdesign.hellopetvendas.remote

import androidx.databinding.BindingAdapter
import android.widget.ImageView
import br.com.hellopetdesign.hellopetvendas.R
import com.squareup.picasso.Picasso
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import android.graphics.BitmapFactory
import java.net.URL


class RetrofitUtils {
    companion object {
        fun retrofit(): Retrofit {
            val client = OkHttpClient.Builder()
                    .addInterceptor(BasicAuthInterceptor("5a6f3dccab25edea68f99490eb57b5d3", "19551075ea85c55a618fa4442d75cec2"))
                    .build()

            return Retrofit.Builder()
                    .baseUrl("https://hello-pet-design.myshopify.com/admin/api/2019-07/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .addConverterFactory(ScalarsConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .client(client)
                    .build()
        }
    }
}

@BindingAdapter("bind:imageUrl")
fun loadImage(view: ImageView, imageUrl: String) {
    if (imageUrl.isEmpty()) return
    Picasso.get()
            .load(imageUrl)
            .placeholder(R.drawable.placeholder)
            .noFade()
            .resize(300, 300)
            .into(view)
}

@BindingAdapter("bind:imageUrlHard")
fun hardLoadImage(view: ImageView, imageUrl: String) {
    if (imageUrl.isEmpty()) return
    val url = URL(imageUrl)
    val bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream())
    view.setImageBitmap(bmp)
}