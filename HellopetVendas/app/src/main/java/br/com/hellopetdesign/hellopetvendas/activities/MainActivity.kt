package br.com.hellopetdesign.hellopetvendas.activities

import androidx.lifecycle.ViewModelProviders
import android.content.Intent
import android.content.res.Configuration
import android.net.Uri
import android.os.Bundle
import android.text.Html
import android.util.Log
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import android.view.MenuItem
import android.view.View
import br.com.hellopetdesign.hellopetvendas.R
import br.com.hellopetdesign.hellopetvendas.utils.createOrderHtml
import br.com.hellopetdesign.hellopetvendas.viewmodels.ClientViewModel
import br.com.hellopetdesign.hellopetvendas.viewmodels.ProductsViewModel
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class MainActivity : AppCompatActivity() {
    private lateinit var mToggle: ActionBarDrawerToggle
    private lateinit var clientModel: ClientViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        setSupportActionBar(toolbar_main)

        clientModel = ViewModelProviders.of(this).get(ClientViewModel::class.java)
        clientModel.updateClients()

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeButtonEnabled(true)

        mToggle = ActionBarDrawerToggle(this,
                drawerLayout,
                android.R.string.ok,
                android.R.string.cancel)

        drawerLayout.addDrawerListener(mToggle)
    }

    //region toggle bs
    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        mToggle.syncState()
    }

    override fun onConfigurationChanged(newConfig: Configuration?) {
        super.onConfigurationChanged(newConfig)
        mToggle.onConfigurationChanged(newConfig)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (mToggle.onOptionsItemSelected(item)) {
            return true
        }
        return super.onOptionsItemSelected(item)
    }
    //endregion

    fun onClickNewOrder(view: View) {
        val intent = Intent(this, OrderActivity::class.java)
        startActivity(intent)
    }

    fun onClickNewClient(view: View) {
        val intent = Intent(this, ClientActivity::class.java)
        startActivity(intent)
    }

    fun onClickClientList(view: View) {
        val intent = Intent(this, ClientListActivity::class.java)
        startActivity(intent)
    }

    fun onClickOrderList(view: View) {
        val intent = Intent(this, OrderListActivity::class.java)
        startActivity(intent)
    }

    fun onClickProducts(view : View) {
        val intent = Intent(this, ProductListActivity::class.java)
        startActivity(intent)
    }
}