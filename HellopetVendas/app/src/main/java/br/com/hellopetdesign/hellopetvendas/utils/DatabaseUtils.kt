package br.com.hellopetdesign.hellopetvendas.utils

import android.content.Context
import android.util.Log
import br.com.hellopetdesign.hellopetvendas.database.AppDatabase
import br.com.hellopetdesign.hellopetvendas.database.model.ProductShopifyEntity
import br.com.hellopetdesign.hellopetvendas.database.model.VariantShopifyEntitiy
import br.com.hellopetdesign.hellopetvendas.remote.models.ProductListShopify
import br.com.hellopetdesign.hellopetvendas.remote.RetrofitUtils
import br.com.hellopetdesign.hellopetvendas.remote.ShopifyService
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch


class DatabaseUtils {
    companion object {
        fun loadDb(context: Context, successRunnable: Runnable, failureRunnable: Runnable) {
            val retrofit = RetrofitUtils.retrofit()

            val shopifyService = retrofit.create(ShopifyService::class.java)

            val requests = ArrayList<Observable<ProductListShopify>>()
            requests.add(shopifyService.listProducts())
            requests.add(shopifyService.listProductsPage2())

            Observable.zip(requests
            ) { objects ->

                val listProductShopifyEntity: ArrayList<ProductShopifyEntity> = ArrayList()
                val listVariantShopifyEntity: ArrayList<VariantShopifyEntitiy> = ArrayList()

                var highestId = 0L

                for (o in objects) {
                    val temp = o as ProductListShopify?

                    if (temp == null) {
                        failureRunnable.run()
                    } else {
                        for (prod in temp.productList) {
                            listProductShopifyEntity.add(ProductShopifyEntity(prod.id, prod.name, prod.getImageUrl()))

                            if(prod.id > highestId) highestId = prod.id

                            for (variant in prod.variantList) {
                                if (variant.name == "Default Title") variant.name = prod.name
                                else variant.name = prod.name + " " + variant.name

                                listVariantShopifyEntity.add(VariantShopifyEntitiy(
                                        variant.id, prod.id, variant.ref, variant.price,
                                        variant.name, variant.barcode))
                            }
                        }
                    }
                }

                Log.d("vgc", "high $highestId")

                GlobalScope.launch {
                    val db = AppDatabase(context)
                    db.productShopifyDao().insert(listProductShopifyEntity)
                    db.variantShopifyDao().insert(listVariantShopifyEntity)

                    PreferencesManager(context).setDownloadedProducts()

                    successRunnable.run()
                }

                objects
            }
                    .subscribeOn(Schedulers.io())
                    .subscribe()
        }
    }
}