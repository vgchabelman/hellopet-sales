package br.com.hellopetdesign.hellopetvendas.database.dao

import android.database.sqlite.SQLiteDatabase
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import br.com.hellopetdesign.hellopetvendas.database.model.VariantShopifyEntitiy

@Dao
interface VariantShopifyDao {
    @Insert(onConflict = SQLiteDatabase.CONFLICT_REPLACE)
    suspend fun insert(variantShopifyEntitiy: VariantShopifyEntitiy)

    @Insert(onConflict = SQLiteDatabase.CONFLICT_REPLACE)
    suspend fun insert(variantShopifyEntitiy: List<VariantShopifyEntitiy>)

    @Query("SELECT * FROM VariantShopifyEntitiy WHERE id = :id")
    suspend fun getVariant(id: Long): VariantShopifyEntitiy

    @Query("SELECT * FROM VariantShopifyEntitiy WHERE prodId = :idProduct")
    suspend fun getVariantsFromProduct(idProduct: Long): List<VariantShopifyEntitiy>

    @Query("SELECT * FROM VariantShopifyEntitiy WHERE barcode = :barcode")
    suspend fun getVariantFromBarcode(barcode: String): VariantShopifyEntitiy
}