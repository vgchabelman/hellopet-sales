package br.com.hellopetdesign.hellopetvendas.database.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class ProductShopifyEntity(@PrimaryKey val id: Long,
                                val title: String?,
                                val imageUrl: String?)