package br.com.hellopetdesign.hellopetvendas.adapters

import android.content.Intent
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import br.com.hellopetdesign.hellopetvendas.activities.OrderActivity
import br.com.hellopetdesign.hellopetvendas.activities.OrderListActivity
import br.com.hellopetdesign.hellopetvendas.databinding.RowOrderBinding
import br.com.hellopetdesign.hellopetvendas.models.Order

class OrderAdapter(val orderListActivity: OrderListActivity) : RecyclerView.Adapter<OrderAdapter.ViewHolder>() {
    private var orders = ArrayList<Order>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val rowOrderBinding = RowOrderBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false)
        return ViewHolder(rowOrderBinding)
    }

    override fun getItemCount(): Int {
        return orders.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(orders[position])
        holder.binding.orderEdit.setOnClickListener {
            val intent = Intent(orderListActivity, OrderActivity::class.java)
            intent.putExtra(OrderActivity.ORDER_KEY, orders[position])
            orderListActivity.startActivity(intent)
        }
    }

    fun updateOrders(orders: ArrayList<Order>) {
        this.orders = orders
        notifyDataSetChanged()
    }

    inner class ViewHolder(val binding: RowOrderBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(order: Order) {
            binding.order = order
            binding.executePendingBindings()
        }
    }
}