package br.com.hellopetdesign.hellopetvendas.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import br.com.hellopetdesign.hellopetvendas.database.dao.ProductShopifyDao
import br.com.hellopetdesign.hellopetvendas.database.dao.VariantShopifyDao
import br.com.hellopetdesign.hellopetvendas.database.model.ProductShopifyEntity
import br.com.hellopetdesign.hellopetvendas.database.model.VariantShopifyEntitiy

@Database(version = 1,
        entities = [
            ProductShopifyEntity::class,
            VariantShopifyEntitiy::class
        ])
abstract class AppDatabase : RoomDatabase() {
    abstract fun productShopifyDao(): ProductShopifyDao
    abstract fun variantShopifyDao(): VariantShopifyDao

    companion object {
        @Volatile
        private var instance: AppDatabase? = null
        private val LOCK = Any()

        operator fun invoke(context: Context) = instance ?: synchronized(LOCK) {
            instance ?: buildDatabase(context).also { instance = it }
        }

        private fun buildDatabase(context: Context) = Room.databaseBuilder(context,
                AppDatabase::class.java, "hellopet.db")
                .build()
    }
}