package br.com.hellopetdesign.hellopetvendas.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import br.com.hellopetdesign.hellopetvendas.models.Order
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

class OrderViewModel : ViewModel() {
    val orders = MutableLiveData<ArrayList<Order>>()

    fun updateOrders(string: String) {
        val database = FirebaseDatabase.getInstance()
        val myRef = database.getReference("orders2")
        myRef.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                val contactChildren = snapshot.children

                val temp = ArrayList<Order>()

                for (contact in contactChildren) {
                    val c = contact.getValue<Order>(Order::class.java)
                    c?.let {
                        if (string == "Todos" || c.client?.representante == string) temp.add(it)
                    }
                }

                temp.reverse()

                orders.postValue(temp)
            }

            override fun onCancelled(error: DatabaseError) {}
        })
    }
}