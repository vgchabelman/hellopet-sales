package br.com.hellopetdesign.hellopetvendas.database.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class VariantShopifyEntitiy(@PrimaryKey val id: Long,
                                 val prodId: Long,
                                 val sku: String,
                                 val price: Double?,
                                 val title: String?,
                                 val barcode: String)