package br.com.hellopetdesign.hellopetvendas.remote.models

import com.google.gson.annotations.SerializedName

data class ProductListShopify(@SerializedName("products") val productList : List<ProductShopify>) {
}