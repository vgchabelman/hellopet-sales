package br.com.hellopetdesign.hellopetvendas.activities

import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.view.View
import br.com.hellopetdesign.hellopetvendas.R
import br.com.hellopetdesign.hellopetvendas.adapters.ClientAdapter
import br.com.hellopetdesign.hellopetvendas.models.Client
import br.com.hellopetdesign.hellopetvendas.viewmodels.ClientViewModel
import kotlinx.android.synthetic.main.activity_client_list.*

class ClientListActivity : AppCompatActivity() {
    lateinit var adapter: ClientAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_client_list)

        adapter = ClientAdapter(this)

        val clientModel = ViewModelProviders.of(this).get(ClientViewModel::class.java)
        clientModel.clients.observe(this, Observer {
            adapter.updateClients(it!!, object : ClientAdapter.OnClientListener {
                override fun onClientChosen(client: Client) {
                    if (intent.getBooleanExtra("forOrder", false)) {
                        OrderActivity.order.client = client
                        this@ClientListActivity.finish()
                    }
                }
            })
        })

        clientList.adapter = adapter

        clientModel.updateClients()
    }

    override fun onResume() {
        super.onResume()

        adapter.notifyDataSetChanged()
    }

    override fun getSupportParentActivityIntent(): Intent? {
        return getParentActivityIntentImpl()
    }

    override fun getParentActivityIntent(): Intent? {
        return getParentActivityIntentImpl()
    }

    private fun getParentActivityIntentImpl(): Intent? {
        if (intent.getBooleanExtra("forOrder", false)) {
            finish()
            return null
        } else {
            val i = Intent(this, MainActivity::class.java)
            i.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP

            return i
        }
    }

    fun onClickNewClient(view: View) {
        val intent = Intent(this, ClientActivity::class.java)
        startActivity(intent)
    }
}