package br.com.hellopetdesign.hellopetvendas.models

import com.google.firebase.database.Exclude
import java.io.Serializable
import java.text.NumberFormat

data class Order(var orderNum: Long = 0,
                 var products: ArrayList<ProductInOrder> = ArrayList(),
                 var client: Client? = null,
                 var observation: String = "",
                 var discount: Int = 0,
                 var freight: Double = 0.0) : Serializable {

    fun getSumPricesFormatted(): String {
        return NumberFormat.getCurrencyInstance().format(getSumPrices())
    }

    @Exclude
    fun getSumPrices() : Double {
        var i = 0.0
        for (p: ProductInOrder in products) {
            i += (p.product!!.price * p.quantity)
        }
        i += freight
        i *= ((100.00 - discount) / 100.00)
        return i
    }

    @Exclude
    fun getFreightFormatted() : String {
        return NumberFormat.getCurrencyInstance().format(freight)
    }

    @Exclude
    fun getDiscountFormatted() : String {
        return "$discount%"
    }

    @Exclude
    fun getOrderNumFormatted() : String {
        return orderNum.toString().substring(orderNum.toString().length - 4)
    }
}