package br.com.hellopetdesign.hellopetvendas.models

import com.google.firebase.database.Exclude
import java.io.Serializable
import java.text.NumberFormat

data class ProductInOrder(val product: Product? = null,
                          var quantity : Int = 1,
                          var observation : String = "") : Serializable {

    @Exclude
    fun getPriceFormatted() : String {
        return NumberFormat.getCurrencyInstance().format(product!!.price * quantity)
    }

    fun getQuantityFormatted() : String {
        if (quantity == 1) return "$quantity unidade"
        return "$quantity unidades"
    }
}