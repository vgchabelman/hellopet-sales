package br.com.hellopetdesign.hellopetvendas.remote

import br.com.hellopetdesign.hellopetvendas.remote.models.ProductListShopify
import io.reactivex.Observable
import retrofit2.http.GET

interface ShopifyService {
    @GET("products.json?limit=250")
    fun listProducts(): Observable<ProductListShopify>

    @GET("products.json?limit=250&page_info=eyJsYXN0X2lkIjoxMjAyNzYxMjM2NTE2LCJsYXN0X3ZhbHVlIjoiTmljaG8gUGFyYSBHYXRvcyBTaWxodWV0YSBWZXJtZWxobyIsImRpcmVjdGlvbiI6Im5leHQifQ%3D%3D")
    fun listProductsPage2(): Observable<ProductListShopify>
}