package br.com.hellopetdesign.hellopetvendas.adapters

import android.app.AlertDialog
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import br.com.hellopetdesign.hellopetvendas.R
import br.com.hellopetdesign.hellopetvendas.activities.OrderActivity
import br.com.hellopetdesign.hellopetvendas.models.ProductInOrder

class ProductsAdapter : RecyclerView.Adapter<ProductsAdapter.ViewHolder>() {
    private var products: ArrayList<ProductInOrder> = ArrayList()
    private lateinit var activity: OrderActivity

    fun updateProducts(activity: OrderActivity, array: ArrayList<ProductInOrder>) {
        products = array
        this.activity = activity
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.row_product, parent, false)

        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return products.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val p = products[position]
        holder.name.text = p.product!!.name
        holder.quantity.text = p.quantity.toString()
        holder.price.text = p.getPriceFormatted()

        holder.plus.setOnClickListener {
            p.quantity++
            holder.quantity.text = p.quantity.toString()
            holder.price.text = p.getPriceFormatted()
            activity.updatePrice()
        }

        holder.minus.setOnClickListener {
            p.quantity--
            if (p.quantity < 1) {
                val builder = AlertDialog.Builder(activity)
                builder.setTitle("REMOVER PRODUTO")
                builder.setPositiveButton("Remover") { _, _ ->
                    OrderActivity.order.products.remove(p)
                    updateProducts(activity, OrderActivity.order.products)
                }
                builder.setNegativeButton("Cancelar") { dialogInterface, _ ->
                    dialogInterface.dismiss()
                }
                builder.create().show()
            } else {
                holder.quantity.text = p.quantity.toString()
                holder.price.text = p.getPriceFormatted()
                activity.updatePrice()
            }
        }
    }

    inner class ViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        var name : TextView = view.findViewById(R.id.productName)
        var quantity : TextView = view.findViewById(R.id.productQuantity)
        var plus : Button = view.findViewById(R.id.productPlus)
        var minus : Button = view.findViewById(R.id.productMinus)
        var price : TextView = view.findViewById(R.id.productPrice)
    }
}