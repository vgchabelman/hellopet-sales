package br.com.hellopetdesign.hellopetvendas.remote.models

import br.com.hellopetdesign.hellopetvendas.models.ImageShopify
import com.google.gson.annotations.SerializedName
import java.text.NumberFormat

data class ProductShopify(val id: Long,
                          @SerializedName("title") val name: String,
                          @SerializedName("body_html") val description: String,
                          @SerializedName("variants") val variantList: List<VariantShopify>,
                          @SerializedName("image") val imageShopify: ImageShopify?,
                          val imageUrlText: String = "test") {

    fun getImageUrl(): String {
        if (imageUrlText != null && imageUrlText.isNotEmpty()) return imageUrlText
        if (imageShopify == null) return ""
        return imageShopify.src
    }

    fun getPrice(): String {
        return NumberFormat.getCurrencyInstance().format(variantList[0].price)
    }
}