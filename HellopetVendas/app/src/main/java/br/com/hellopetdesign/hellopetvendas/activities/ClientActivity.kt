package br.com.hellopetdesign.hellopetvendas.activities

import android.content.Context
import androidx.databinding.DataBindingUtil
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import android.view.MenuItem
import android.view.View
import android.widget.EditText
import br.com.hellopetdesign.hellopetvendas.R
import br.com.hellopetdesign.hellopetvendas.databinding.ActivityClientBinding
import br.com.hellopetdesign.hellopetvendas.models.Client
import com.google.firebase.database.FirebaseDatabase
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_client.*
import java.util.*

class ClientActivity : AppCompatActivity() {
    companion object {
        const val CLIENT_KEY = "CLIENT_KEY"
        const val UPDATE_KEY = "UPDATE_KEY"
    }

    private var isUpdating: Boolean = false
    private lateinit var client: Client

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        isUpdating = intent.getBooleanExtra(UPDATE_KEY, false)

        if (isUpdating) {
            client = intent.getSerializableExtra(CLIENT_KEY) as Client
        } else {
            client = Client()
        }

        val clientBind = DataBindingUtil.setContentView<ActivityClientBinding>(this, R.layout.activity_client)
        clientBind.client = client
        clientBind.executePendingBindings()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                confirmExit()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        confirmExit()
    }

    private fun confirmExit() {
        val alertBuilder = AlertDialog.Builder(this)
        alertBuilder.setTitle("DESEJA SAIR SEM SALVAR?")
        alertBuilder.setPositiveButton("SAIR") { _, _ ->
            finish()
        }
        alertBuilder.setNegativeButton("CANCELAR", null)
        alertBuilder.create().show()
    }

    fun onClickSave(view: View) {
        if (razaoSocial.text.toString().isEmpty()) {
            return alertEmptyField(razaoSocial.hint.toString())
        }

        val client = Client(razaoSocial = razaoSocial.text.toString(),
                nomeFantasia = nomeFantasia.text.toString(),
                cnpj = getLongValue(cnpj),
                inscricaoEstadual = inscricaoEstadual.text.toString(),
                endereco = endereco.text.toString(),
                bairro = bairro.text.toString(),
                cep = getLongValue(cep),
                cidade = cidade.text.toString(),
                estado = estado.text.toString(),
                tel = getLongValue(tel),
                whatsapp = getLongValue(whatsapp),
                comprador = comprador.text.toString(),
                email = email.text.toString(),
                createdAt = Calendar.getInstance().timeInMillis)

        val json = Gson().toJson(client)

        val sharedPreferences = getSharedPreferences("hellopet.clients", Context.MODE_PRIVATE)
        sharedPreferences.edit().putString(client.razaoSocial, json).apply()

        val database = FirebaseDatabase.getInstance()
        val myRef = database.getReference("clients")
        val clientRef = myRef.child(razaoSocial.text.toString())
        clientRef.setValue(client)

        finish()
    }

    private fun alertEmptyField(campo: String) {
        val alertDialogBuilder = AlertDialog.Builder(this)
        alertDialogBuilder.setTitle("Campo obrigatório")
        alertDialogBuilder.setMessage("O campo $campo está vazio")
        alertDialogBuilder.setCancelable(true)
        alertDialogBuilder.setPositiveButton("OK") { dialogInterface, _ -> dialogInterface.dismiss() }
        alertDialogBuilder.create().show()
    }

    private fun getLongValue(text: EditText): Long {
        return try {
            text.text.toString().toLong()
        } catch (e: NumberFormatException) {
            0
        }
    }
}