package br.com.hellopetdesign.hellopetvendas.activities

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Canvas
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import br.com.hellopetdesign.hellopetvendas.R
import br.com.hellopetdesign.hellopetvendas.databinding.ActivityOrderFinishedBinding
import br.com.hellopetdesign.hellopetvendas.databinding.ViewProductBinding
import br.com.hellopetdesign.hellopetvendas.models.Order
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.single.PermissionListener
import kotlinx.android.synthetic.main.activity_order_finished.*
import java.io.File
import java.util.*
import kotlin.collections.ArrayList

class OrderFinishedActivity : AppCompatActivity() {
    companion object {
        const val FINISHER_KEY = "FINISHED_KEY"
    }
    lateinit var order : Order

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        order = intent.getSerializableExtra(FINISHER_KEY) as Order

        val binding = DataBindingUtil.setContentView(this, R.layout.activity_order_finished) as ActivityOrderFinishedBinding
        binding.order = order

        for (p in order.products) {
            val bindingProduct = DataBindingUtil.inflate(
                    LayoutInflater.from(this@OrderFinishedActivity),
                    R.layout.view_product,
                    orderFinishProducts,
                    true) as ViewProductBinding

            bindingProduct.product = p
        }
    }

    fun onClickShare(view: View) {
        val bitmap = loadBitmapFromView(orderFinishContainer)

        if (ContextCompat.checkSelfPermission(this@OrderFinishedActivity, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            Dexter.withActivity(this@OrderFinishedActivity)
                    .withPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    .withListener(object : PermissionListener {
                        override fun onPermissionGranted(response: PermissionGrantedResponse?) {
                            shareImg(bitmap)
                        }

                        override fun onPermissionRationaleShouldBeShown(permission: PermissionRequest?, token: PermissionToken?) {
                        }

                        override fun onPermissionDenied(response: PermissionDeniedResponse?) {
                        }

                    }).check()
        } else {
            orderFinishContainer.post {
                shareImg(bitmap)
            }
        }
    }

    private fun shareImg(bitmap: Bitmap) {
        val imgSaved = MediaStore.Images.Media.insertImage(
                contentResolver,
                bitmap,
                "order num" + order.orderNum + ".png", "drawing")

        val uri = Uri.parse(imgSaved)

        val shareIntent = Intent(Intent.ACTION_SEND)
        shareIntent.type = "image/png"
        shareIntent.putExtra(Intent.EXTRA_STREAM, uri)
        startActivity(Intent.createChooser(shareIntent, "Enviar"))
    }

    private fun loadBitmapFromView(view: View): Bitmap {
        val bitmap = Bitmap.createBitmap(view.measuredWidth,
                view.measuredHeight, Bitmap.Config.ARGB_8888)
        val bitmapHolder = Canvas(bitmap)
        view.draw(bitmapHolder)
        return bitmap
    }
}