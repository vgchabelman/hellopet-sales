package br.com.hellopetdesign.hellopetvendas.adapters

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import br.com.hellopetdesign.hellopetvendas.databinding.RowProductShopifyBinding
import br.com.hellopetdesign.hellopetvendas.remote.models.ProductShopify

class ProductShopifyAdapter() : RecyclerView.Adapter<ProductShopifyAdapter.ViewHolder>() {
    private var productList: List<ProductShopify> = ArrayList()

    fun updateProductList(productList: List<ProductShopify>) {
        this.productList = productList
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val productBinding = RowProductShopifyBinding.inflate(layoutInflater, null, false)
        return ViewHolder(productBinding)
    }

    override fun getItemCount(): Int {
        return productList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(productList[position])
    }

    inner class ViewHolder(var productShopifyBinding: RowProductShopifyBinding) :
            RecyclerView.ViewHolder(productShopifyBinding.root) {

        fun bind(productShopify: ProductShopify) {
            productShopifyBinding.productShopify = productShopify
            productShopifyBinding.executePendingBindings()
        }
    }
}