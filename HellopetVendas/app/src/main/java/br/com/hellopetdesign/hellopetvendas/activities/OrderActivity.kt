package br.com.hellopetdesign.hellopetvendas.activities

import android.content.Intent
import androidx.databinding.DataBindingUtil
import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import android.text.Editable
import android.text.InputFilter
import android.text.InputType
import android.text.TextWatcher
import android.view.MenuItem
import android.view.View
import android.widget.EditText
import android.widget.TextView
import br.com.hellopetdesign.hellopetvendas.R
import br.com.hellopetdesign.hellopetvendas.adapters.ProductsAdapter
import br.com.hellopetdesign.hellopetvendas.databinding.ActivityOrderBinding
import br.com.hellopetdesign.hellopetvendas.models.Order
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_order.*
import java.math.BigDecimal
import java.text.NumberFormat
import java.util.*


class OrderActivity : AppCompatActivity() {
    companion object {
        var order = Order()
        const val ORDER_KEY = "ORDER_KEY"
    }

    lateinit var adapter: ProductsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val editing = intent?.getSerializableExtra(ORDER_KEY)
        if (editing != null) {
            order = editing as Order
        }

        val binding = DataBindingUtil.setContentView(this, R.layout.activity_order) as ActivityOrderBinding
        binding.order = order

        adapter = ProductsAdapter()

        productsRecycler.adapter = adapter
    }

    override fun onResume() {
        super.onResume()

        adapter.updateProducts(this, order.products)

        if (order.client == null) clientName.text = "ESCOLHER CLIENTE"
        else clientName.text = order.client!!.razaoSocial

        orderFreight.text = NumberFormat.getCurrencyInstance().format(order.freight)
        orderDiscount.text = "${order.discount}%"

        updatePrice()
    }

    override fun onDestroy() {
        super.onDestroy()

        order = Order()
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            // Respond to the action bar's Up/Home button
            android.R.id.home -> {
                confirmExit()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        confirmExit()
    }

    private fun confirmExit() {
        val alertBuilder = AlertDialog.Builder(this)
        alertBuilder.setTitle("DESEJA SAIR SEM FINALIZAR?")
        alertBuilder.setPositiveButton("SAIR") { _, _ ->
            finish()
        }
        alertBuilder.setNegativeButton("CANCELAR", null)
        alertBuilder.create().show()
    }

    fun updatePrice() {
        totalOrder.text = order.getSumPricesFormatted()
    }

    fun onClickSelectClient(view: View) {
        val intent = Intent(this, ClientListActivity::class.java)
        intent.putExtra("forOrder", true)
        startActivity(intent)
    }

    fun onClickAdd(view: View) {
        val intent = Intent(this, ScanActivity::class.java)
        startActivity(intent)
    }

    fun onClickFreight(view: View) {
        val input = EditText(this@OrderActivity)
        input.inputType = InputType.TYPE_CLASS_NUMBER
        input.width = 300
        input.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(editable: Editable?) {
                val s = editable.toString()
                if (s.isEmpty()) return
                input.removeTextChangedListener(this)
                val cleanString = s.replace("[R$,.]".toRegex(), "").replace(" ", "")
                val parsed = BigDecimal(cleanString).setScale(2, BigDecimal.ROUND_FLOOR).divide(BigDecimal(100), BigDecimal.ROUND_FLOOR)
                val formatted = NumberFormat.getCurrencyInstance().format(parsed)
                input.setText(formatted)
                input.setSelection(formatted.length)
                input.addTextChangedListener(this)
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
        })

        val builder = AlertDialog.Builder(this@OrderActivity)
        builder.setTitle("FRETE")
        builder.setMessage("Digite o valor do frete")
        builder.setView(input)
        builder.setPositiveButton("Inserir Frete") { dialogInterface, _ ->
            val freight = input.text.toString().replace("[R$.]".toRegex(), "").replace(",", ".").replace(" ", "").toDouble()
            order.freight = freight
            (view as TextView).text = input.text
            updatePrice()
            dialogInterface.dismiss()
        }
        builder.create().show()
    }

    fun onClickDiscount(view: View) {
        val input = EditText(this@OrderActivity)
        input.inputType = InputType.TYPE_CLASS_NUMBER
        input.width = 300
        val filterArray = Array<InputFilter>(1) { _: Int -> InputFilter.LengthFilter(3) }
        input.filters = filterArray

        val builder = AlertDialog.Builder(this@OrderActivity)
        builder.setTitle("DESCONTO")
        builder.setMessage("Digite a porcentagem do desconto")
        builder.setView(input)
        builder.setPositiveButton("Inserir desconto") { dialogInterface, _ ->
            val discount = input.text.toString().toInt()
            if (discount <= 100) {
                order.discount = discount
                (view as TextView).text = "$discount%"
                updatePrice()
                dialogInterface.dismiss()
            } else {
                input.text.clear()
                input.hint = "O valor deve ser entre 0 e 100"
            }
        }
        builder.create().show()
    }

    fun onClickFinish(view: View) {
        if (order.client != null && order.products.isNotEmpty()) {
            val input = EditText(this@OrderActivity)
            input.width = 300
            input.hint = "Observações"
            input.setText(order.observation, TextView.BufferType.EDITABLE)

            val builder = AlertDialog.Builder(this@OrderActivity)
            builder.setTitle("FINALIZAR")
            builder.setMessage("Você quer finalizar esse pedido?")
            builder.setCancelable(false)
            builder.setView(input)
            builder.setPositiveButton(android.R.string.ok) { _, _ ->
                order.observation = input.text.toString()

                if (order.orderNum == 0L) {
                    order.orderNum = Calendar.getInstance().timeInMillis;
                }

                val database = FirebaseDatabase.getInstance()
                val myRef = database.getReference("orders2")
                val orderRef = myRef.child(order.orderNum.toString())
                orderRef.setValue(order)

                val intent = Intent(this@OrderActivity, OrderFinishedActivity::class.java)
                intent.putExtra(OrderFinishedActivity.FINISHER_KEY, order)
                startActivity(intent)
            }
            builder.setNegativeButton(android.R.string.cancel) { dialogInterface, _ ->
                dialogInterface.dismiss()
            }
            builder.create().show()
        } else {
            Snackbar.make(rootOrder,
                    "Preencha o Cliente e a lista de Produtos",
                    Snackbar.LENGTH_LONG).show()
        }
    }
}