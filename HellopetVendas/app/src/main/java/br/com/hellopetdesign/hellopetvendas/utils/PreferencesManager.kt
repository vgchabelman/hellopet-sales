package br.com.hellopetdesign.hellopetvendas.utils

import android.content.Context
import android.content.SharedPreferences

class PreferencesManager() {
    private lateinit var sharedPreferences : SharedPreferences

    companion object {
        @Volatile
        private var instance: PreferencesManager? = null
        private val LOCK = Any()

        operator fun invoke(context: Context) = instance ?: synchronized(LOCK) {
            instance ?: setupSharedPreferences(context).also { instance = it }
        }

        private fun setupSharedPreferences(context: Context) : PreferencesManager {
            val p = PreferencesManager()
            p.sharedPreferences = context.getSharedPreferences("hellopet-pref", Context.MODE_PRIVATE)

            return p
        }
    }

    fun setDownloadedProducts() {
        sharedPreferences.edit().putBoolean("DOWNLOADED_PRODUCTS", true).apply()
    }

    fun hasDownloadedProducts() = sharedPreferences.getBoolean("DOWNLOADED_PRODUCTS", false)
}