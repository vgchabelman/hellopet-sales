package br.com.hellopetdesign.hellopetvendas.models

import java.io.Serializable

data class Product(val barcode: String  = "00000000",
                   val name: String = "default",
                   val ref: String = "default",
                   val price: Double = 0.00,
                   val imageUrl: String? = "") : Serializable {
}