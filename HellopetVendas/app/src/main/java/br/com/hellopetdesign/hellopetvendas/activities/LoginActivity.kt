package br.com.hellopetdesign.hellopetvendas.activities

import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import android.view.View
import android.view.inputmethod.EditorInfo
import br.com.hellopetdesign.hellopetvendas.BuildConfig
import br.com.hellopetdesign.hellopetvendas.R
import br.com.hellopetdesign.hellopetvendas.utils.DatabaseUtils
import br.com.hellopetdesign.hellopetvendas.utils.PasswordManager
import br.com.hellopetdesign.hellopetvendas.utils.PreferencesManager
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_login)

        password.setOnEditorActionListener { textView, i, _ ->
            if (i == EditorInfo.IME_ACTION_DONE) {
                handlePassword(textView.text.toString().toLowerCase())
                return@setOnEditorActionListener true
            }

            return@setOnEditorActionListener false
        }
    }

    fun onClickLogin(view: View) {
        handlePassword(password.text.toString().toLowerCase())
    }

    private fun handlePassword(string: String) {
        if (PasswordManager.isPassword(string)) {
            PasswordManager.currentActivePassword = string

            if (PreferencesManager(this).hasDownloadedProducts()) {
                val i = Intent(this, MainActivity::class.java)
                i.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP
                startActivity(i)
                finish()
                return
            }

            val progressDialog = ProgressDialog(this)
            progressDialog.setTitle("Carregando produtos, por favor espere")
            progressDialog.setCancelable(false)

            DatabaseUtils.loadDb(this, Runnable {
                runOnUiThread {
                    progressDialog.dismiss()

                    val i = Intent(this, MainActivity::class.java)
                    i.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP
                    startActivity(i)
                    finish()
                }
            }, Runnable {
                Snackbar.make(rootLogin, "Erro ao atualizar", Snackbar.LENGTH_LONG).show()
                progressDialog.dismiss()
            })

            progressDialog.show()
        } else {
            Snackbar.make(rootLogin, "TENTE NOVAMENTE", Snackbar.LENGTH_LONG).show()
        }
    }
}