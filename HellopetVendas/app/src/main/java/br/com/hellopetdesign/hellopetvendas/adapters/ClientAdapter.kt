package br.com.hellopetdesign.hellopetvendas.adapters

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import br.com.hellopetdesign.hellopetvendas.activities.ClientActivity
import br.com.hellopetdesign.hellopetvendas.databinding.RowClientBinding
import br.com.hellopetdesign.hellopetvendas.models.Client

class ClientAdapter(var activity: AppCompatActivity) : RecyclerView.Adapter<ClientAdapter.ViewHolder>() {
    private var clients = ArrayList<Client>()
    private lateinit var click: OnClientListener

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val rowClientBinding = RowClientBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false)
        return ViewHolder(rowClientBinding)
    }

    override fun getItemCount(): Int {
        return clients.size
    }

    fun updateClients(temp: ArrayList<Client>, clientListener: OnClientListener) {
        clients = temp
        click = clientListener
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(clients[position])
        holder.itemView.setOnClickListener {
            click.onClientChosen(clients[position])
        }
        holder.itemView.setOnLongClickListener {
            val intent = Intent(activity, ClientActivity::class.java)
            intent.putExtra(ClientActivity.UPDATE_KEY, true)
            intent.putExtra(ClientActivity.CLIENT_KEY, clients[position])
            activity.startActivity(intent)
            true
        }
    }

    inner class ViewHolder(val binding: RowClientBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(client: Client) {
            binding.client = client
            binding.executePendingBindings()
        }
    }

    interface OnClientListener {
        fun onClientChosen(client: Client)
    }
}