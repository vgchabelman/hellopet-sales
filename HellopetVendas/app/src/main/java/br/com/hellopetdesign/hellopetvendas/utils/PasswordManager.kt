package br.com.hellopetdesign.hellopetvendas.utils

class PasswordManager {
    companion object {
        val USERS = arrayOf("Nathalia", "Adriana", "Deborah", "Christina", "Julio", "Mariane")
        const val NATHALIA = "mia"
        const val ADRIANA = "luna"
        const val DEBORA = "rio"
        const val CRIS = "pet4us"
        const val JULIO = "italia"
        const val MARIANE = "gato"

        var currentActivePassword = ""

        fun isAdmin() : Boolean {
            return currentActivePassword == NATHALIA || currentActivePassword == ADRIANA
        }

        fun  isPassword(string: String) : Boolean {
            return string == NATHALIA || string == ADRIANA || string == DEBORA || string == CRIS || string == JULIO || string == MARIANE
        }

        fun currentUser() : String {
            when (currentActivePassword) {
                NATHALIA -> return USERS[0]
                ADRIANA -> return USERS[1]
                DEBORA -> return USERS[2]
                CRIS -> return USERS[3]
                JULIO -> return USERS[4]
                MARIANE -> return USERS[5]
            }
            return "Nathalia"
        }
    }
}