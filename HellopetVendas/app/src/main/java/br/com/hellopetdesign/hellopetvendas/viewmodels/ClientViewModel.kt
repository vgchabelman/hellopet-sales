package br.com.hellopetdesign.hellopetvendas.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import br.com.hellopetdesign.hellopetvendas.models.Client
import br.com.hellopetdesign.hellopetvendas.utils.PasswordManager
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

class ClientViewModel : ViewModel() {
    val clients = MutableLiveData<ArrayList<Client>>()

    fun updateClients() {
        val database = FirebaseDatabase.getInstance()
        val myRef = database.getReference("clients")
        myRef.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
//                clients.postValue(snapshot.value as List<Client>?)
                val contactChildren = snapshot.children
                val temp = ArrayList<Client>()

                for (contact in contactChildren) {
                    val c = contact.getValue<Client>(Client::class.java)
                    c?.let {
                        if (PasswordManager.isAdmin() || it.representante == PasswordManager.currentUser()) {
                            temp.add(it)
                        }
                    }
                }

                temp.sortBy { selector(it) }
                temp.reverse()

                clients.postValue(temp)
            }

            override fun onCancelled(error: DatabaseError) {}
        })
    }

    fun selector(c: Client): Long = c.createdAt
}