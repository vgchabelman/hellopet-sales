package br.com.hellopetdesign.hellopetvendas.models

import android.telephony.PhoneNumberUtils
import br.com.hellopetdesign.hellopetvendas.utils.PasswordManager
import com.google.firebase.database.Exclude
import java.io.Serializable

data class Client(val razaoSocial: String = "",
                  val nomeFantasia: String = "",
                  val cnpj: Long? = null,
                  val inscricaoEstadual: String = "",
                  val endereco: String = "",
                  val bairro: String = "",
                  val cep: Long? = null,
                  val cidade: String = "",
                  val estado: String = "",
                  val tel: Long? = null,
                  val whatsapp: Long? = null,
                  val comprador: String = "",
                  val email: String = "",
                  val representante: String = PasswordManager.currentUser(),
                  val createdAt: Long = 0
) : Serializable {

    @Exclude
    fun getCnpjFormated(): String {
        val str = cnpj.toString()

        var result = ""

        try {
            result = "${str.subSequence(0, 2)}.${str.subSequence(2, 5)}.${str.subSequence(5, 8)}/${str.subSequence(8, 12)}-${str.subSequence(12, 14)}"
        } catch (e: Exception) {
        }


        return result
    }

    @Exclude
    fun getCepFormatted(): String {
        val str = cep.toString()

        var result = ""

        try {
            result = "${str.subSequence(0, 5)}-${str.subSequence(5, 8)}"
        } catch (e: Exception) {
        }

        return result
    }

    @Exclude
    fun getTelFomatted(): String {
        if (tel == null || tel == 0L) return ""
        return PhoneNumberUtils.formatNumber(tel.toString(), "BR")
    }

    @Exclude
    fun getWhasappFormatted(): String {
        if (whatsapp == null || whatsapp == 0L) return ""
        return PhoneNumberUtils.formatNumber(whatsapp.toString(), "BR")
    }

    @Exclude
    fun getEmpty() : String {
        return ""
    }

    @Exclude
    fun getClientNameFomatted() :String {
        if (nomeFantasia.isEmpty()) return razaoSocial

        return nomeFantasia
    }
}