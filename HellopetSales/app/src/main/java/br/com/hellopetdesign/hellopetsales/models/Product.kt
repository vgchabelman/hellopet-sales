package br.com.hellopetdesign.hellopetsales.models

import java.io.Serializable

data class Product(val id: Int,
                   val name: String,
                   val ref: String,
                   val price: Double) : Serializable {
}