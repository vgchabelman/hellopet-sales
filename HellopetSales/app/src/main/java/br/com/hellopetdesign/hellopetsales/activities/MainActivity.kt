package br.com.hellopetdesign.hellopetsales.activities

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import br.com.hellopetdesign.hellopetsales.R
import br.com.hellopetdesign.hellopetsales.adapters.HomeTabsAdapter
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        setSupportActionBar(toolbar)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        viewpager.adapter = HomeTabsAdapter(supportFragmentManager)

        tabs.setupWithViewPager(viewpager)

//        val barcodeReader = supportFragmentManager.findFragmentById(R.id.barcode_fragment) as BarcodeReader
//
//        barcodeReader.setListener(object : BarcodeReader.BarcodeReaderListener {
//            override fun onBitmapScanned(sparseArray: SparseArray<Barcode>?) {}
//
//            override fun onScannedMultiple(barcodes: MutableList<Barcode>?) {}
//
//            override fun onCameraPermissionDenied() {
//                Toast.makeText(applicationContext,
//                        "Por favor aceite a permissão para scannear",
//                        Toast.LENGTH_SHORT).show()
//            }
//
//            override fun onScanned(barcode: Barcode?) {
//                barcodeReader.playBeep()
//                Log.d("vgc", barcode!!.displayValue)
//                Toast.makeText(applicationContext,
//                        barcode!!.displayValue,
//                        Toast.LENGTH_SHORT).show()
//                barcodeReader.pauseScanning()
//            }
//
//            override fun onScanError(errorMessage: String?) {
//                Log.e("vgc", errorMessage)
//            }
//        })
    }

    public fun onClickNewOrder(view : View) {

    }
}