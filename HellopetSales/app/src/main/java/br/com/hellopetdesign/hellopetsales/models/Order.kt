package br.com.hellopetdesign.hellopetsales.models

data class Order(val products: List<Product>,
                 val client: Client) {
    fun getSumPrices(): Double {
        var i = 0.0
        for (p : Product in products) {
            i += p.price
        }
        return i
    }
}