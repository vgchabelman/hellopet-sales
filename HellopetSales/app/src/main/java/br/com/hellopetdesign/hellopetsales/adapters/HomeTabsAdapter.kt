package br.com.hellopetdesign.hellopetsales.adapters

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import br.com.hellopetdesign.hellopetsales.fragments.ClientsFragment
import br.com.hellopetdesign.hellopetsales.fragments.HomeFragment
import br.com.hellopetdesign.hellopetsales.fragments.OrdersFragment

class HomeTabsAdapter(fm: FragmentManager?) : FragmentPagerAdapter(fm) {
    override fun getItem(position: Int): Fragment {
        when (position) {
            0 -> return HomeFragment()
            1 -> return ClientsFragment()
            2 -> return OrdersFragment()
        }
        return HomeFragment()
    }

    override fun getCount(): Int {
        return 3
    }

    override fun getPageTitle(position: Int): CharSequence? {
        when (position) {
            0 -> return "Home"
            1 -> return "Clientes"
            2 -> return "Pedidos"
        }
        return ""
    }
}