package br.com.hellopetdesign.hellopetsales.models

data class Client(val razaoSocial : String,
                  val nomeFantasia : String,
                  val CNPJ: Int,
                  val inscricaoEstadual: Int,
                  val endereco : String,
                  val bairro : String,
                  val CEP : Int,
                  val cidade : String,
                  val estado : String,
                  val tel : Int,
                  val responsavel : String
                  ) {
}